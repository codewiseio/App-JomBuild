package App::JomBuild;

use Moose;
use MooseX::StrictConstructor;
use MooseX::SemiAffordanceAccessor;


use Archive::Zip;

use Concept::Log::Global;

use File::Copy::Recursive qw(rcopy);
use File::Find;
use File::Path qw(rmtree);
use File::Slurp qw(read_file append_file);
use Getopt::Long qw(:config no_ignore_case );
use Config::Tiny;


use File::Basename qw(fileparse);
use File::Spec;


# configuration file name
has 'inifile' => (
    is => 'rw',
    isa => 'Str',
    default => 'dist.ini'
);

# do not include extension and images folders
has 'minimal' => (
    is => 'rw',
    isa => 'Bool',
    default => 0
);

# source directory
has 'source' => (
    is => 'rw',
    isa => 'Str',
    default => '.'
);

has 'output' => (
    is => 'rw',
    isa => 'Str|Undef',
);

## print comments
#has 'verbose' => (
#    is => 'rw',
#    isa => 'Bool',
#    default => '1',
#);







sub run {
    my ( $self ) = @_;
    
    # change to source directory
    #if ( $self->source ne '.' ) {
    #    chirp ( "Changing directory to " . $self->source );
    #    
    #    chdir( $self->source );
    #}
    
    
    my $inipath = File::Spec->catfile( $self->source, $self->inifile );
    
    # check for ini file
    if ( ! $self->inifile || ! -f $inipath ) {
        chirp( error => "Could not find dist.ini" );
        exit;
    }
    
    # load configuration
    my $cfg = Config::Tiny->new->read( $inipath );
    
    # check configuration file
    if ( ! $cfg->{'_'}{name} ) {
        chirp ( error =>  "Could not find component name in configuration.");
        die;
    }
    
    # create temporary directory name
    my $tmpdir = $cfg->{'_'}{name};
    $tmpdir .= '-' . $cfg->{'_'}{version} if $cfg->{'_'}{version};
    
    # delete temp folder from failed/previous build
    chirp ( 'Removing previous build directory: ' . $tmpdir );
    if ( -d $tmpdir ) {
        rmtree( $tmpdir ) or chirp ( error => "Could not delete $tmpdir from previous build.") and exit;
    }
    
    # create a temporary directory
    chirp ( 'Creating build directory: ' . $tmpdir );
    mkdir $tmpdir or chirp( error => "Could not create temporary directory.\n" ) and exit;
    
    # copy files to temporary directory
    my @sources = glob( $self->source . '/*');
    
    
    chirp ( 'Copying files to build directory: ' . $tmpdir );
    for my $source ( @sources ) {
        
        # exclude directories and files starting with . (.git) or _ (_overwrite)
        next if $source =~ /^.*[\\\/](\.|_)[^\\\/]+$/;
        next if $source =~ /^.*[\\\/]$tmpdir(\.zip)?[^\\\/]*$/;
        next if $source =~ /\.komodo.*$/;
        next if $source =~ /.ini$/;
        
        # perform recursive copy
        if ( -e $source ) {
            
            my $target = $source;
            my $source_path = quotemeta( $self->source );
            
            $target =~ s/^$source_path[\\\/]//;
            
           rcopy( $source, $tmpdir . '\\' . $target ) or die "Could not copy file $source.\n$!\n";
        }
        
    }
    
    # change into the temporary directory
    chirp ( "Entering build directory" );
    chdir $tmpdir or chirp( error => "Could not change to temp directory.") and exit;
    
    for ( 'site', 'admin' ) {
        
        chirp ( "Merging $_ language files" );
        
        
        # merge language files
        if (-d "$_/language") {
            my @langfiles = glob("$_/language/*.ini");
            
            for my $source ( @langfiles ) {
                
                # look for language file in the fork directory
                my $filename = fileparse($source);
                my $forkfile = "$_/fork/language/$filename";
                
                if ( -f $forkfile ) {
                    
                    # read append text
                    my $append_text = read_file( $forkfile );
                    
                    # write to original lang
                    append_file( $source, $append_text );
                    
                    # delete additional lang file
                    unlink $forkfile;
                }
                
            }
        }
        
        # delete _fork folders to decrease archive size
        # this is not the "fork" folder which holds actual forked code, the _fork folder contains unused code provided from J-Cook
        chirp ( "Removing $_ _fork directory files" );
        if (-d "$_/_fork" ) {
            rmtree( "$_/_fork" ) or chirp ( error => "Could not delete _fork folder") and exit;
        }
        
        # if running in min-mode, delete image folders to decrease file size
        
        if ( $self->minimal  ) {
            chirp ( "Minimal mode: removing $_ images directory" );
            map { -d $_ ? rmtree($_) : unlink $_ } glob ("$_/images/*");
        }
      
    }
    
    # if running in minamal mode, remove image folders and "extensions" directory to decrease archive size
    if ( $self->minimal ) {
        chirp ( "Minimal mode: removing $_ extensions directory" );
        rmtree("extensions");
    }

    # change into the parent directory
    chdir '..';
    
    my $output_file = $self->output || $tmpdir . '.zip';
    
    # delete last output file if exists
    unlink $output_file if -f $output_file;
    
    chirp ('Creating distribution: ' . $output_file );
    
    my $zip = Archive::Zip->new;
    
    $zip->addDirectory($tmpdir);
    
    $zip->addTree( $tmpdir, $tmpdir, undef, 9 );
    
    $zip->writeToFileNamed( $output_file );
    
    if ( ! -f $output_file ) {
        chirp ( error => 'Error creating ' . $output_file ) and exit;
    }
    
    chirp ('Removing build directory ' . $output_file );
    
    rmtree($tmpdir);

}


1;



__END__

=pod

=head1 NAME

App::JomBuild - Utility for packaging Joomla components created with J-cook

=head1 DESCRIPTION

Utility for packaging Joomla components created with J-cook

=head1 AUTHOR

Jeffrey Ray Hallock E<lt>jeffrey.hallock@gmail.com<gt>

=head1 COPYRIGHT & LICENSE

This software is Copyright (c) 2013-2015 Jeffrey Ray Hallock.

Artistic_2_0
