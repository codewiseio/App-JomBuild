CREATE TABLE IF NOT EXISTS `#__blank_foos` (
	`id` int(11) NOT NULL auto_increment,
	`label` VARCHAR(255) NOT NULL ,
	`bar` INT(11) ,
	`created_by` INT(11) ,
	`modified_by` INT(11) ,
	`creation_date` DATETIME ,
	`modification_date` DATETIME ,
	`published` INT(11) ,

	PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__blank_bars` (
	`id` int(11) NOT NULL auto_increment,
	`label` VARCHAR(255) NOT NULL ,
	`description` TEXT ,
	`created_by` INT(11) ,
	`modified_by` INT(11) ,
	`creation_date` DATETIME ,
	`modification_date` DATETIME ,
	`published` INT(11) ,

	PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



