<?php
/**                               ______________________________________________
*                          o O   |                                              |
*                 (((((  o      <    Generated with Cook Self Service  V2.6.5   |
*                ( o o )         |______________________________________________|
* --------oOOO-----(_)-----OOOo---------------------------------- www.j-cook.pro --- +
* @version		1.0
* @package		Blank
* @subpackage	
* @copyright	2015 Jeffrey Hallock
* @author		Jeffrey Hallock -  - codewise.cc@gmail.com
* @license		Artistic 2.0
*
*             .oooO  Oooo.
*             (   )  (   )
* -------------\ (----) /----------------------------------------------------------- +
*               \_)  (_/
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Some usefull constants
if(!defined('DS')) define('DS',DIRECTORY_SEPARATOR);
if(!defined('BR')) define("BR", "<br />");
if(!defined('LN')) define("LN", "\n");

//Joomla 1.6 only
if (!defined('JPATH_PLATFORM')) define('JPATH_PLATFORM', JPATH_SITE .DS. 'libraries');

// Main component aliases
if (!defined('COM_BLANK')) define('COM_BLANK', 'com_blank');
if (!defined('BLANK_CLASS')) define('BLANK_CLASS', 'Blank');

// Component paths constants
if (!defined('JPATH_ADMIN_BLANK')) define('JPATH_ADMIN_BLANK', JPATH_ADMINISTRATOR . DS . 'components' . DS . COM_BLANK);
if (!defined('JPATH_SITE_BLANK')) define('JPATH_SITE_BLANK', JPATH_SITE . DS . 'components' . DS . COM_BLANK);

// JQuery use
if(!defined('JQUERY_VERSION')) define('JQUERY_VERSION', '1.8.2');


$app = JFactory::getApplication();
jimport('joomla.version');
$version = new JVersion();

// Load the component Dependencies
require_once(dirname(__FILE__) .DS. 'helper.php');


require_once(dirname(__FILE__) .DS. '..' .DS. 'classes' .DS. 'loader.php');

BlankClassLoader::setup(false, false);
BlankClassLoader::discover('Blank', JPATH_ADMIN_BLANK, false, true);

// Some helpers
BlankClassLoader::register('JToolBarHelper', JPATH_ADMINISTRATOR .DS. "includes" .DS. "toolbar.php", true);

if (version_compare($version->RELEASE, '3.0', '<'))
	BlankClassLoader::register('JSubMenuHelper', JPATH_ADMINISTRATOR .DS. "includes" .DS. "toolbar.php", true);

// Handle cross compatibilities
require_once(dirname(__FILE__) .DS. 'mvc.php');

// Set the models directory
if ($app->isSite())
CkJController::addModelPath(JPATH_SITE_BLANK .DS.'models');
else
CkJController::addModelPath(JPATH_ADMIN_BLANK .DS.'models');


//Instance JDom
if (!isset($app->dom))
{
	jimport('jdom.dom');
	if (!class_exists('JDom'))
		jexit('JDom plugin is required');

	JDom::getInstance();	
}
