<?php
/**                               ______________________________________________
*                          o O   |                                              |
*                 (((((  o      <    Generated with Cook Self Service  V2.6.5   |
*                ( o o )         |______________________________________________|
* --------oOOO-----(_)-----OOOo---------------------------------- www.j-cook.pro --- +
* @version		1.0
* @package		Blank
* @subpackage	
* @copyright	2015 Jeffrey Hallock
* @author		Jeffrey Hallock -  - codewise.cc@gmail.com
* @license		Artistic 2.0
*
*             .oooO  Oooo.
*             (   )  (   )
* -------------\ (----) /----------------------------------------------------------- +
*               \_)  (_/
*/


// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
if(!defined('DS')) define('DS',DIRECTORY_SEPARATOR);

//Copy this line to be able to call the application from outside (Module, Plugin, Third component, ...)
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_blank'.DS.'helpers'.DS.'loader.php');

//Document title
$document	= JFactory::getDocument();
$document->titlePrefix = JText::_('BLANK_BLANK') . ' - ';
$document->titleSuffix = '';

if (defined('JDEBUG') && count($_POST))
	$_SESSION['Blank']['$_POST'] = $_POST;

$jinput = JFactory::getApplication()->input;

$controller = CkJController::getInstance('Blank');
$controller->execute($jinput->get('task', null, 'CMD'));
$controller->redirect();
