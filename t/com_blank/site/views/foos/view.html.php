<?php
/**                               ______________________________________________
*                          o O   |                                              |
*                 (((((  o      <    Generated with Cook Self Service  V2.6.5   |
*                ( o o )         |______________________________________________|
* --------oOOO-----(_)-----OOOo---------------------------------- www.j-cook.pro --- +
* @version		1.0
* @package		Blank
* @subpackage	Foos
* @copyright	2015 Jeffrey Hallock
* @author		Jeffrey Hallock -  - codewise.cc@gmail.com
* @license		Artistic 2.0
*
*             .oooO  Oooo.
*             (   )  (   )
* -------------\ (----) /----------------------------------------------------------- +
*               \_)  (_/
*/

// no direct access
defined('_JEXEC') or die('Restricted access');



/**
* HTML View class for the Blank component
*
* @package	Blank
* @subpackage	Foos
*/
class BlankCkViewFoos extends BlankClassView
{
	/**
	* Execute and display a template script.
	*
	* @access	public
	* @param	string	$tpl	The name of the template file to parse; automatically searches through the template paths.
	*
	* @return	mixed	A string if successful, otherwise a JError object.
	*
	* @since	11.1
	*/
	public function display($tpl = null)
	{
		$layout = $this->getLayout();
		if (!in_array($layout, array('default')))
			JError::raiseError(0, $layout . ' : ' . JText::_('JERROR_LAYOUT_PAGE_NOT_FOUND'));

		$fct = "display" . ucfirst($layout);

		$this->addForkTemplatePath();
		$this->$fct($tpl);			
		$this->_parentDisplay($tpl);
	}

	/**
	* Execute and display a template : Foos
	*
	* @access	protected
	* @param	string	$tpl	The name of the template file to parse; automatically searches through the template paths.
	*
	* @return	mixed	A string if successful, otherwise a JError object.
	*
	* @since	11.1
	*/
	protected function displayDefault($tpl = null)
	{
		$this->model		= $model	= $this->getModel();
		$this->state		= $state	= $this->get('State');
		$this->params 		= $state->get('params');
		$state->set('context', 'foos.default');
		$this->items		= $items	= $this->get('Items');
		$this->canDo		= $canDo	= BlankHelper::getActions();
		$this->pagination	= $this->get('Pagination');
		$this->filters = $filters = $model->getForm('default.filters');
		$this->menu = BlankHelper::addSubmenu('foos', 'default');
		$lists = array();
		$this->lists = &$lists;

		// Define the default title
		$this->params->def('title', JText::_('BLANK_LAYOUT_FOOS'));

		$this->_prepareDocument();

		// Deprecated var : use $this->params->get('page_heading')
		$this->title = $this->params->get('page_heading');


		

		//Filters
		// Bar > Label
		$modelBar = CkJModel::getInstance('bars', 'BlankModel');
		$filters['filter_bar']->jdomOptions = array(
			'list' => $modelBar->getItems()
		);

		// Sort by
		$filters['sortTable']->jdomOptions = array(
			'list' => $this->getSortFields('default')
		);

		// Limit
		$filters['limit']->jdomOptions = array(
			'pagination' => $this->pagination
		);

		//Toolbar initialization


		// New
		if ($model->canCreate())
			CkJToolBarHelper::addNew('foo.add', "BLANK_JTOOLBAR_NEW");

		// Edit
		if ($model->canEdit())
			CkJToolBarHelper::editList('foo.edit', "BLANK_JTOOLBAR_EDIT");

		// Delete
		if ($model->canDelete())
			CkJToolBarHelper::deleteList(JText::_('BLANK_JTOOLBAR_ARE_YOU_SURE_TO_DELETE'), 'foo.delete', "BLANK_JTOOLBAR_DELETE");
	}

	/**
	* Execute and display a template : Foos
	*
	* @access	protected
	* @param	string	$tpl	The name of the template file to parse; automatically searches through the template paths.
	*
	* @return	mixed	A string if successful, otherwise a JError object.
	*
	* @since	11.1
	*/
	protected function displayModal($tpl = null)
	{
		$this->model		= $model	= $this->getModel();
		$this->state		= $state	= $this->get('State');
		$this->params 		= $state->get('params');
		$state->set('context', 'foos.modal');
		$this->items		= $items	= $this->get('Items');
		$this->canDo		= $canDo	= BlankHelper::getActions();
		$this->pagination	= $this->get('Pagination');
		$this->filters = $filters = $model->getForm('modal.filters');
		$this->menu = BlankHelper::addSubmenu('foos', 'modal');
		$lists = array();
		$this->lists = &$lists;

		// Define the default title
		$this->params->def('title', JText::_('BLANK_LAYOUT_FOOS'));

		$this->_prepareDocument();

		// Deprecated var : use $this->params->get('page_heading')
		$this->title = $this->params->get('page_heading');


		

		//Filters
		// Bar > Label
		$modelBar = CkJModel::getInstance('bars', 'BlankModel');
		$filters['filter_bar']->jdomOptions = array(
			'list' => $modelBar->getItems()
		);

		// Limit
		$filters['limit']->jdomOptions = array(
			'pagination' => $this->pagination
		);

		//Toolbar initialization


		// New
		if ($model->canCreate())
			CkJToolBarHelper::addNew('foo.add', "BLANK_JTOOLBAR_NEW");

		// Edit
		if ($model->canEdit())
			CkJToolBarHelper::editList('foo.edit', "BLANK_JTOOLBAR_EDIT");

		// Delete
		if ($model->canDelete())
			CkJToolBarHelper::deleteList(JText::_('BLANK_JTOOLBAR_ARE_YOU_SURE_TO_DELETE'), 'foo.delete', "BLANK_JTOOLBAR_DELETE");


	}

	/**
	* Returns an array of fields the table can be sorted by.
	*
	* @access	protected
	* @param	string	$layout	The name of the called layout. Not used yet
	*
	* @return	array	Array containing the field name to sort by as the key and display text as value.
	*
	* @since	3.0
	*/
	protected function getSortFields($layout = null)
	{
		return array(
			'_bar_.label' => JText::_('BLANK_FIELD_BAR')
		);
	}


}

// Load the fork
BlankHelper::loadFork(__FILE__);

// Fallback if no fork has been found
if (!class_exists('BlankViewFoos')){ class BlankViewFoos extends BlankCkViewFoos{} }

