<?php
/**                               ______________________________________________
*                          o O   |                                              |
*                 (((((  o      <    Generated with Cook Self Service  V2.6.5   |
*                ( o o )         |______________________________________________|
* --------oOOO-----(_)-----OOOo---------------------------------- www.j-cook.pro --- +
* @version		1.0
* @package		Blank
* @subpackage	Bars
* @copyright	2015 Jeffrey Hallock
* @author		Jeffrey Hallock -  - codewise.cc@gmail.com
* @license		Artistic 2.0
*
*             .oooO  Oooo.
*             (   )  (   )
* -------------\ (----) /----------------------------------------------------------- +
*               \_)  (_/
*/

// no direct access
defined('_JEXEC') or die('Restricted access');



?>
<fieldset class="fieldsfly fly-horizontal">
	<legend><?php echo JText::_('BLANK_FIELDSET_FLY') ?></legend>

	<div class="control-group field-label">
    	<div class="control-label">
			<label><?php echo JText::_( "BLANK_FIELD_LABEL" ); ?></label>
		</div>
		
        <div class="controls">
			<?php echo JDom::_('html.fly', array(
				'dataKey' => 'label',
				'dataObject' => $this->item
			));?>
		</div>
    </div>
	<div class="control-group field-description">
    	<div class="control-label">
			<label><?php echo JText::_( "BLANK_FIELD_DESCRIPTION" ); ?></label>
		</div>
		
        <div class="controls">
			<?php echo JDom::_('html.fly', array(
				'dataKey' => 'description',
				'dataObject' => $this->item
			));?>
		</div>
    </div>

</fieldset>
