<?php
/**                               ______________________________________________
*                          o O   |                                              |
*                 (((((  o      <    Generated with Cook Self Service  V2.6.5   |
*                ( o o )         |______________________________________________|
* --------oOOO-----(_)-----OOOo---------------------------------- www.j-cook.pro --- +
* @version		1.0
* @package		Blank
* @subpackage	Foos
* @copyright	2015 Jeffrey Hallock
* @author		Jeffrey Hallock -  - codewise.cc@gmail.com
* @license		Artistic 2.0
*
*             .oooO  Oooo.
*             (   )  (   )
* -------------\ (----) /----------------------------------------------------------- +
*               \_)  (_/
*/

// no direct access
defined('_JEXEC') or die('Restricted access');



?>
<fieldset class="fieldsfly fly-horizontal">
	<legend><?php echo JText::_('BLANK_FIELDSET_FLY') ?></legend>

	<div class="control-group field-label">
    	<div class="control-label">
			<label><?php echo JText::_( "BLANK_FIELD_LABEL" ); ?></label>
		</div>
		
        <div class="controls">
			<?php echo JDom::_('html.fly', array(
				'dataKey' => 'label',
				'dataObject' => $this->item
			));?>
		</div>
    </div>
	<div class="control-group field-_bar_label">
    	<div class="control-label">
			<label><?php echo JText::_( "BLANK_FIELD_BAR" ); ?></label>
		</div>
		
        <div class="controls">
			<?php echo JDom::_('html.fly', array(
				'dataKey' => '_bar_label',
				'dataObject' => $this->item
			));?>
		</div>
    </div>
	<div class="control-group field-_created_by_username">
    	<div class="control-label">
			<label><?php echo JText::_( "BLANK_FIELD_USERNAME" ); ?></label>
		</div>
		
        <div class="controls">
			<?php echo JDom::_('html.fly', array(
				'dataKey' => '_created_by_username',
				'dataObject' => $this->item
			));?>
		</div>
    </div>
	<div class="control-group field-creation_date">
    	<div class="control-label">
			<label><?php echo JText::_( "BLANK_FIELD_CREATION_DATE" ); ?></label>
		</div>
		
        <div class="controls">
			<?php echo JDom::_('html.fly.datetime', array(
				'dataKey' => 'creation_date',
				'dataObject' => $this->item,
				'dateFormat' => 'Y-m-d H:i'
			));?>
		</div>
    </div>
	<div class="control-group field-published">
    	<div class="control-label">
			<label><?php echo JText::_( "BLANK_FIELD_PUBLISHED" ); ?></label>
		</div>
		
        <div class="controls">
			<?php echo JDom::_('html.fly.publish', array(
				'dataKey' => 'published',
				'dataObject' => $this->item
			));?>
		</div>
    </div>

</fieldset>
