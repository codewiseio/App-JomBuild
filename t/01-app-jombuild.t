

use Test::More qw(tests 3);


use_ok 'App::JomBuild';

my $app = App::JomBuild->new( source => 't/com_blank');

isa_ok $app, 'App::JomBuild';

$app->run();

ok -f 'com_blank-1.0.zip', 'Created distribution archive';